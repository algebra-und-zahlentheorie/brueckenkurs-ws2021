\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{uebblatt}[2013/03/29 LaTeX class]

\LoadClass[a4paper,ngerman]{scrartcl}

\RequirePackage[utf8]{inputenc}
\RequirePackage[ngerman]{babel}
\RequirePackage{amsmath,amsthm,amssymb,amscd,color,graphicx,environ}
\RequirePackage{mathtools}
\RequirePackage[protrusion=true,expansion=true]{microtype}
\RequirePackage{multicol}
\RequirePackage{xspace}
\RequirePackage{wrapfig}
\RequirePackage{hyperref}
\RequirePackage{tikz}

\usetikzlibrary{matrix}
\usetikzlibrary{positioning}
\usetikzlibrary{circuits.ee.IEC}
\usetikzlibrary{circuits.logic.IEC}

\RequirePackage{lmodern}
%\RequirePackage[T1]{fontenc}
%\RequirePackage{libertine}

\RequirePackage{geometry}
\geometry{tmargin=1.0cm,bmargin=2cm,lmargin=2.5cm,rmargin=2.5cm}

\setlength\parskip{\medskipamount}
\setlength\parindent{0pt}

\newcounter{blattnummer}
\newlength{\titleskip}
\setlength{\titleskip}{1.5em}
\addtocounter{blattnummer}{-1}
\newenvironment{blatt}{
  \clearpage
  \setcounter{aufgabennummer}{0}
  \addtocounter{blattnummer}{1}
  \thispagestyle{empty}
  Universität Augsburg \hfill Marc Nieper-Wißkirchen \\
  Lehrstuhl für Algebra und Zahlentheorie \hfill Ingo Blechschmidt \\
  Wintersemester 2021/22

  \begin{center}\Large \textbf{Übungsblatt \theblattnummer{} zum Brückenkurs} \\[1em]
  \end{center}
  \vspace{\titleskip}
}{}

\newenvironment{entwurf}{
  \begin{blatt}
    \vspace*{-1.2em}
    {\centering\huge\textbf{ENTWURF}\par\bigskip}
}{\end{blatt}}

\renewcommand*\theenumi{\alph{enumi}}
\renewcommand{\labelenumi}{(\theenumi)}

\newlength{\aufgabenskip}
\setlength{\aufgabenskip}{1.4em}
\newcounter{aufgabennummer}
\newenvironment{aufgabe}[1]{
  \addtocounter{aufgabennummer}{1}
  \textbf{Aufgabe \theblattnummer.\theaufgabennummer.} \emph{#1} \par
}{\par\vspace{\aufgabenskip}}

\newlength{\sternchenlength}
\settowidth{\sternchenlength}{$\star$ }
\newenvironment{aufgabe*}[1]{
  \addtocounter{aufgabennummer}{1}
  \hspace{-\sternchenlength}$\star$ \textbf{Aufgabe \theblattnummer.\theaufgabennummer.} \emph{#1} \par
}{\par\vspace{\aufgabenskip}}

\clubpenalty=10000
\widowpenalty=10000
\displaywidowpenalty=10000

\newcommand{\QQ}{\mathbb{Q}}
\newcommand{\RR}{\mathbb{R}}
\newcommand{\CC}{\mathbb{C}}
\newcommand{\ZZ}{\mathbb{Z}}
\newcommand{\NN}{\mathbb{N}}
\newcommand{\PP}{\mathbb{P}}
\newcommand{\aaa}{\mathfrak{a}}
\newcommand{\im}{\mathrm{i}}
\newcommand{\defeq}{\vcentcolon=}
